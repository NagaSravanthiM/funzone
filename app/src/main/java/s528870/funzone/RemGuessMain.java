package s528870.funzone;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.concurrent.ThreadLocalRandom;

public class RemGuessMain extends AppCompatActivity {

    int[] alphabets = {R.drawable.a, R.drawable.b, R.drawable.c, R.drawable.d, R.drawable.f, R.drawable.g, R.drawable.h, R.drawable.i,
            R.drawable.j, R.drawable.k, R.drawable.m, R.drawable.o, R.drawable.p, R.drawable.q, R.drawable.r, R.drawable.s, R.drawable.t,
            R.drawable.v, R.drawable.w, R.drawable.x, R.drawable.y, R.drawable.z};

    int imageIndex = 0;
    String[] stringArray = {"","","",""};

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_rem_guess_main);
        getSupportActionBar().hide();
        shuffleAlphaImages(alphabets);
       // assignImages();

    }

    public void goToNextActivity(View v){
        Intent in = new Intent(this, RemGuessNext.class);
        startActivity(in);
    }

    public void goToNextlevel(View v){
        Intent in = new Intent(this, FinishGame.class);
        startActivity(in);
    }

    public void goTopreviousActivity(View v){
        Intent in = new Intent(this, Level4EnterActivity.class);
        startActivity(in);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void shuffleAlphaImages(int[] arrayVal) {
        System.out.println("in shuffle images");
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        for (int i = arrayVal.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = arrayVal[index];
            arrayVal[index] = arrayVal[i];
            arrayVal[i] = a;
        }

        for (int i : arrayVal) {
            System.out.println("Arrayvalues");
            System.out.println(i + " ");
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void assignImages() {
        ImageView img1 = (ImageView) findViewById(R.id.a00);
        img1.setImageResource(alphabets[0]);
        img1.setTag("0");
        ImageView img2 = (ImageView) findViewById(R.id.a01);
        img2.setImageResource(alphabets[1]);
        img2.setTag("1");
        ImageView img3 = (ImageView) findViewById(R.id.a02);
        img3.setImageResource(alphabets[2]);
        img3.setTag("2");
        ImageView img4 = (ImageView) findViewById(R.id.a10);
        img4.setImageResource(alphabets[3]);
        img4.setTag("3");
        ImageView img5 = (ImageView) findViewById(R.id.a11);
        img5.setImageResource(alphabets[4]);
        img5.setTag("4");
        ImageView img6 = (ImageView) findViewById(R.id.a12);
        img6.setImageResource(alphabets[5]);
        img6.setTag("5");
        ImageView img7 = (ImageView) findViewById(R.id.a20);
        img7.setImageResource(alphabets[6]);
        img7.setTag("6");
        ImageView img8 = (ImageView) findViewById(R.id.a21);
        img8.setImageResource(alphabets[7]);
        img8.setTag("7");
        ImageView img9 = (ImageView) findViewById(R.id.a22);
        img9.setImageResource(alphabets[8]);
        img9.setTag("8");
        System.out.println("in assignimages");
        for (int i : alphabets) {
            System.out.println("alphabets");
            System.out.println(i);

        }

    }

    public void start(){

    }

    public void hide(View v){
        TextView txtView = (TextView)findViewById(R.id.hideShow);

        txtView.setVisibility(TextView.INVISIBLE);    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, Music.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, Music.class));
    }
}
