package s528870.funzone;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sun);
        //mediaPlayer.start();
        startService(new Intent(this, Music.class));

 }

    public void goToNextActivity(View v){
        Intent in = new Intent(this, Level1EnterActivity.class);
        startActivity(in);
    }
}
