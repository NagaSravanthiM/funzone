package s528870.funzone;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.concurrent.ThreadLocalRandom;

public class Sequencing extends AppCompatActivity {

    int[] alphabets = {R.drawable.a, R.drawable.b, R.drawable.c, R.drawable.d, R.drawable.f, R.drawable.g, R.drawable.h, R.drawable.i,
            R.drawable.j, R.drawable.k, R.drawable.m, R.drawable.o, R.drawable.p, R.drawable.q, R.drawable.r, R.drawable.s, R.drawable.t,
            R.drawable.v, R.drawable.w, R.drawable.x, R.drawable.y, R.drawable.z};

    int imageIndex = 0;
    String[] stringArray = {"","","",""};

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sequencing);
        getSupportActionBar().hide();
        shuffleAlphaImages(alphabets);
        assignImages();

    }

    public void goToNextActivity(View v) {
        Intent in = new Intent(this, Level4EnterActivity.class);
        startActivity(in);
    }

    public void goToNext() {
        Intent in = new Intent(this, Level4EnterActivity.class);
        startActivity(in);
    }

    public void goTopreviousActivity(View v) {
        Intent in = new Intent(this, Level3EnterActivity.class);
        startActivity(in);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void shuffleAlphaImages(int[] arrayVal) {
        System.out.println("in shuffle images");
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        for (int i = arrayVal.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = arrayVal[index];
            arrayVal[index] = arrayVal[i];
            arrayVal[i] = a;
        }

        for (int i : arrayVal) {
            System.out.println(i + " ");
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void assignImages() {
        ImageView img1 = (ImageView) findViewById(R.id.image1);
        img1.setImageResource(alphabets[0]);
        img1.setTag("0");
        ImageView img2 = (ImageView) findViewById(R.id.image2);
        img2.setImageResource(alphabets[1]);
        img2.setTag("1");
        ImageView img3 = (ImageView) findViewById(R.id.image3);
        img3.setImageResource(alphabets[2]);
        img3.setTag("2");
        ImageView img4 = (ImageView) findViewById(R.id.image4);
        img4.setImageResource(alphabets[3]);
        img4.setTag("3");
        System.out.println("in assignimages");



    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void imageClicked(View v) {
        ImageView img = (ImageView) findViewById(v.getId());
        int b = Integer.valueOf(img.getTag().toString());
        System.out.println("in imageindex "+imageIndex);
        switch (imageIndex) {

            case 0:
                ImageView img1 = (ImageView) findViewById(R.id.first1);
                img1.setImageResource(alphabets[b]);
                populateStringArray(alphabets[b],imageIndex);
                imageIndex++;
                break;
            case 1:
                ImageView img2 = (ImageView) findViewById(R.id.second1);
                img2.setImageResource(alphabets[b]);
                populateStringArray(alphabets[b],imageIndex);
                imageIndex++;
                break;
            case 2:
                ImageView img3 = (ImageView) findViewById(R.id.third1);
                img3.setImageResource(alphabets[b]);
                populateStringArray(alphabets[b],imageIndex);
                imageIndex++;
                break;
            case 3:
                ImageView img4 = (ImageView) findViewById(R.id.fourth1);
                img4.setImageResource(alphabets[b]);
                populateStringArray(alphabets[b],imageIndex);
                imageIndex++;
                break;
        }
        if (imageIndex == 4) {
            if(checkImages()){
                System.out.println("alphas in reverse order");
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        goToNext();
                    }
                }, 500);

            } else{
                final TextView retry = (TextView) findViewById(R.id.retry);
                final GridLayout gl = (GridLayout) findViewById(R.id.grid);

                gl.setVisibility(View.INVISIBLE);
                retry.setVisibility(View.VISIBLE);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        retry.setVisibility(View.INVISIBLE);
                        gl.setVisibility(View.VISIBLE);
                    }
                }, 1000);

                assignImages();
                emptyResult();

            }
        }

    }

    public boolean checkImages() {
        System.out.println("in check images");
        for(String i:stringArray){

            System.out.println(i);
        }

        boolean reverse=true;
        for (int i = 0; i < stringArray.length; i++) {
            for (int j = i + 1; j < stringArray.length; j++) {
                System.out.println("string"+stringArray[i]+stringArray[j]);
                System.out.println(stringArray[i].compareTo(stringArray[j]));
                if (stringArray[i].compareTo(stringArray[j]) < 0) {
                    reverse= false;
                    j=stringArray.length;
                    i=j;
                }

            }
        }
      return reverse;
    }

   public void populateStringArray(int alpha,int imageIndex){
       System.out.println("in pop string");
        switch(alpha){
            case R.drawable.a :
                stringArray[imageIndex]="A";
                break;
            case R.drawable.b:
                stringArray[imageIndex]="B";

                break;
            case R.drawable.c :
                stringArray[imageIndex]="C";

                break;
            case R.drawable.d :
                stringArray[imageIndex]="D";

                break;
            case R.drawable.f :
                stringArray[imageIndex]="F";

                break;
            case R.drawable.g :
                stringArray[imageIndex]="G";

                break;
            case R.drawable.h :
                stringArray[imageIndex]="H";

                break;
            case R.drawable.i :
                stringArray[imageIndex]="I";

                break;
            case R.drawable.j :
                stringArray[imageIndex]="J";
                break;
            case R.drawable.k :
                stringArray[imageIndex]="K";
                break;
            case R.drawable.m :
                stringArray[imageIndex]="M";
                break;
            case R.drawable.o :
                stringArray[imageIndex]="O";
                break;
            case R.drawable.p :
                stringArray[imageIndex]="P";
                break;
            case R.drawable.q :
                stringArray[imageIndex]="Q";
                break;
            case R.drawable.r :
                stringArray[imageIndex]="R";
                break;
            case R.drawable.s :
                stringArray[imageIndex]="S";
                break;
            case R.drawable.t :
                stringArray[imageIndex]="T";
                break;
            case R.drawable.v :
                stringArray[imageIndex]="V";
                break;
            case R.drawable.w :
                stringArray[imageIndex]="W";
                break;
            case R.drawable.x :
                stringArray[imageIndex]="X";
                break;
            case R.drawable.y :
                stringArray[imageIndex]="Y";
                break;
            case R.drawable.z :
                stringArray[imageIndex]="Z";
                break;
        }

    }

    public void emptyResult(){
        ImageButton first1 = (ImageButton) findViewById(R.id.first1);
        ImageButton second1 = (ImageButton) findViewById(R.id.second1);
        ImageButton third1 = (ImageButton) findViewById(R.id.third1);
        ImageButton fourth1 = (ImageButton) findViewById(R.id.fourth1);

        first1.setImageDrawable(null);
        second1.setImageDrawable(null);
        third1.setImageDrawable(null);
        fourth1.setImageDrawable(null);

        imageIndex=0;

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, Music.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, Music.class));
    }


}
