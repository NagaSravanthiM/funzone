package s528870.funzone;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.concurrent.ThreadLocalRandom;

public class TestAlphabet extends AppCompatActivity {


    int[] alphaImages = {R.drawable.apple1,R.drawable.ball,R.drawable.cat1,R.drawable.dog,R.drawable.frog,
            R.drawable.grapes,R.drawable.hen,R.drawable.icecream,R.drawable.joker,R.drawable.kangroo,R.drawable.mango,
            R.drawable.orange,R.drawable.panda,R.drawable.queen,R.drawable.rocket,R.drawable.snake,R.drawable.tiger,
            R.drawable.vegetables,R.drawable.watermelon,R.drawable.xmastree,R.drawable.yalk,R.drawable.zebra};
    int[] alphabets = {R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.f,R.drawable.g,R.drawable.h,R.drawable.i,
            R.drawable.j,R.drawable.k,R.drawable.m,R.drawable.o,R.drawable.p,R.drawable.q,R.drawable.r,R.drawable.s,R.drawable.t,
            R.drawable.v,R.drawable.w,R.drawable.x,R.drawable.y,R.drawable.z};

    int[] setAlphabets = {0,0,0};
    int alphaClick=0;
    int imgClick=0;
    ImageView imgV1, imgV2;
    Bitmap b1,b2;
    int[] tagAlpha, tagImage;
    int matchedImagesPending=6;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_alphabet);
        getSupportActionBar().hide();

        shuffleAlphaImages(alphaImages);
        assignImages();

    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void shuffleAlphaImages(int[] arrayVal){
        System.out.println("in shuffle images");
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        for (int i = arrayVal.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = arrayVal[index];
            arrayVal[index] = arrayVal[i];
            arrayVal[i] = a;
        }

        for(int i:arrayVal){
            System.out.println(i+" ");
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void assignImages() {
        ImageView img1 = (ImageView) findViewById(R.id.match1);
        img1.setImageResource(alphaImages[0]);
        img1.setTag("0");
        ImageView img2 = (ImageView) findViewById(R.id.match2);
        img2.setImageResource(alphaImages[1]);
        img2.setTag("1");
        ImageView img3 = (ImageView) findViewById(R.id.match3);
        img3.setImageResource(alphaImages[2]);
        img3.setTag("2");
        System.out.println("in assignimages");

        for (int i = 0; i < 3; i++) {
            System.out.println("in for loop: "+alphaImages[i]);
            switch (alphaImages[i]){
                case R.drawable.apple1 :
                    setAlphabets[i]=R.drawable.a;

                    break;
                case R.drawable.ball :
                    setAlphabets[i]=R.drawable.b;

                    break;
                case R.drawable.cat1 :
                    setAlphabets[i]=R.drawable.c;

                    break;
                case R.drawable.dog :
                    setAlphabets[i]=R.drawable.d;

                    break;
                case R.drawable.frog :
                    setAlphabets[i]=R.drawable.f;

                    break;
                case R.drawable.grapes :
                    setAlphabets[i]=R.drawable.g;

                    break;
                case R.drawable.hen :
                    setAlphabets[i]=R.drawable.h;

                    break;
                case R.drawable.icecream :
                    setAlphabets[i]=R.drawable.i;

                    break;
                case R.drawable.joker :
                    setAlphabets[i]=R.drawable.j;
                    break;
                case R.drawable.kangroo :
                    setAlphabets[i]=R.drawable.k;
                    break;
                case R.drawable.mango :
                    setAlphabets[i]=R.drawable.m;
                    break;
                case R.drawable.orange :
                    setAlphabets[i]=R.drawable.o;
                    break;
                case R.drawable.panda :
                    setAlphabets[i]=R.drawable.p;
                    break;
                case R.drawable.queen :
                    setAlphabets[i]=R.drawable.q;
                    break;
                case R.drawable.rocket :
                    setAlphabets[i]=R.drawable.r;
                    break;
                case R.drawable.snake :
                    setAlphabets[i]=R.drawable.s;
                    break;
                case R.drawable.tiger :
                    setAlphabets[i]=R.drawable.t;
                    break;
                case R.drawable.vegetables :
                    setAlphabets[i]=R.drawable.v;
                    break;
                case R.drawable.watermelon :
                    setAlphabets[i]=R.drawable.w;
                    break;
                case R.drawable.xmastree :
                    setAlphabets[i]=R.drawable.x;
                    break;
                case R.drawable.yalk :
                    setAlphabets[i]=R.drawable.y;
                    break;
                case R.drawable.zebra :
                    setAlphabets[i]=R.drawable.z;
                    break;
            }
        }

        shuffleAlphaImages(setAlphabets);
        setAlphabetsOnScreen();
    }

    public void setAlphabetsOnScreen(){
        ImageView img1 = (ImageView) findViewById(R.id.alpha1);
        img1.setImageResource(setAlphabets[0]);
        img1.setTag("0");
        ImageView img2 = (ImageView) findViewById(R.id.alpha2);
        img2.setImageResource(setAlphabets[1]);
        img2.setTag("1");
        ImageView img3 = (ImageView) findViewById(R.id.alpha3);
        img3.setImageResource(setAlphabets[2]);
        img3.setTag("2");

    }


    public void aphabetClicked(View v){
        alphaClick=1;
        imgV1 = (ImageView) findViewById(v.getId());

    }

    public void imageClicked(View v){
        if(alphaClick==1) {
            imgClick = 1;
            imgV2 = (ImageView) findViewById(v.getId());
            if(checkImagesMatch()){
                setImageviewsToNull();
                alphaClick=0;
            }
        }
    }

    public boolean checkImagesMatch() {
        System.out.println("in checkImage");
        int alpID = setAlphabets[Integer.valueOf(imgV1.getTag().toString())];
        int imgID = alphaImages[Integer.valueOf(imgV2.getTag().toString())];
        boolean matched=false;
        switch(imgID){
            case R.drawable.apple1 :
                if(alpID==R.drawable.a){
                    matched=true;
                }
                break;
            case R.drawable.ball :
                if(alpID==R.drawable.b){
                    matched=true;
                }
                break;
            case R.drawable.cat1 :
                if(alpID==R.drawable.c){
                    matched=true;
                }
                break;
            case R.drawable.dog :
                if(alpID==R.drawable.d){
                    matched=true;
                }
                break;
            case R.drawable.frog :
                if(alpID==R.drawable.f){
                    matched=true;
                }
                break;
            case R.drawable.grapes :
                if(alpID==R.drawable.g){
                    matched=true;
                }
                break;
            case R.drawable.hen :
                if(alpID==R.drawable.h){
                    matched=true;
                }
                break;
            case R.drawable.icecream :
                if(alpID==R.drawable.i){
                    matched=true;
                    setImageviewsToNull();
                }
                break;
            case R.drawable.joker :
                if(alpID==R.drawable.j){
                    matched=true;
                }
                break;
            case R.drawable.kangroo :
                if(alpID==R.drawable.k){
                    matched=true;
                }
                break;
            case R.drawable.mango :
                if(alpID==R.drawable.m){
                    matched=true;
                }
                break;
            case R.drawable.orange :
                if(alpID==R.drawable.o){
                    matched=true;
                }
                break;
            case R.drawable.panda :
                if(alpID==R.drawable.p){
                    matched=true;
                }
                break;
            case R.drawable.queen :
                if(alpID==R.drawable.q){
                    matched=true;
                }
                break;
            case R.drawable.rocket :
                if(alpID==R.drawable.r){
                    matched=true;
                }
                break;
            case R.drawable.snake :
                if(alpID==R.drawable.s){
                    matched=true;
                }
                break;
            case R.drawable.tiger :
                if(alpID==R.drawable.t){
                    matched=true;
                }
                break;
            case R.drawable.vegetables :
                if(alpID==R.drawable.v){
                    matched=true;
                }
                break;
            case R.drawable.watermelon :
                if(alpID==R.drawable.w){
                    matched=true;
                }
                break;
            case R.drawable.xmastree :
                if(alpID==R.drawable.x){
                    matched=true;
                }
                break;
            case R.drawable.yalk :
                if(alpID==R.drawable.y){
                    matched=true;
                }
                break;
            case R.drawable.zebra :
                if(alpID==R.drawable.z){
                    matched=true;
                }
                break;
        }


     return matched;
        }


        public void setImageviewsToNull(){
            matchedImagesPending-=2;
            imgV1.setImageDrawable(null);
            imgV2.setImageDrawable(null);
            imgV1.setClickable(false);
            imgV2.setClickable(false);
            System.out.println("images matched");
            if(matchedImagesPending==0){

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        goToNext();
                    }
                }, 500);

            }
        }

    public void goToNextActivity(View v){
        Intent in = new Intent(this, Level3EnterActivity.class);
        startActivity(in);
    }

    public void goTopreviousActivity(View v){
        Intent in = new Intent(this, Level2EnterActivity.class);
        startActivity(in);
    }

    public void goToNext(){
        Intent in = new Intent(this, Level3EnterActivity.class);
        startActivity(in);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, Music.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, Music.class));
    }

}
