package s528870.funzone;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ThreadLocalRandom;


public class RemGuessNext extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rem_guess_next);
        getSupportActionBar().hide();

    }

    public void goToNextActivity(View v){
        Intent in = new Intent(this, FinishGame.class);
        startActivity(in);
    }

    public void goTopreviousActivity(View v){
        Intent in = new Intent(this, RemGuessMain.class);
        startActivity(in);
    }



    public void evaluateResult(View v) {
        EditText a00 = (EditText) findViewById(R.id.a00);
        EditText a01 = (EditText) findViewById(R.id.a01);
        EditText a02 = (EditText) findViewById(R.id.a02);
        EditText a10 = (EditText) findViewById(R.id.a10);
        EditText a11 = (EditText) findViewById(R.id.a11);
        EditText a12 = (EditText) findViewById(R.id.a12);
        EditText a20 = (EditText) findViewById(R.id.a20);
        EditText a21 = (EditText) findViewById(R.id.a21);
        EditText a22 = (EditText) findViewById(R.id.a22);

        if (a00.getText().toString().equalsIgnoreCase("X") && a01.getText().toString().equalsIgnoreCase("B") &&
                a02.getText().toString().equalsIgnoreCase("V") && a10.getText().toString().equalsIgnoreCase("M") &&
                a11.getText().toString().equalsIgnoreCase("Y") && a12.getText().toString().equalsIgnoreCase("G") &&
                a20.getText().toString().equalsIgnoreCase("H") && a21.getText().toString().equalsIgnoreCase("J") &&
                a22.getText().toString().equalsIgnoreCase("K")) {
            Intent in = new Intent(this, FinishGame.class);
            startActivity(in);

        } else {
            Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
            Intent in = new Intent(this, RemGuessMain.class);


            startActivity(in);
        }
    }



    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, Music.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, Music.class));
    }
}
