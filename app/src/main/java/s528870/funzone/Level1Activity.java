package s528870.funzone;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.concurrent.ThreadLocalRandom;

public class Level1Activity extends AppCompatActivity {

    public int compareFlag =0;
    public ImageView imageV1, imageV2, actualImageV1, actualImageV2;
    public Drawable d1, d2;
    public Bitmap b1,b2;
    public int totalMatchesPending=12;


    int[] images = {   R.drawable.puppy,
            R.drawable.puppy,
            R.drawable.cat,
            R.drawable.cat,
            R.drawable.apple,
            R.drawable.apple,
            R.drawable.sun,
            R.drawable.sun,
            R.drawable.candy,
            R.drawable.candy,
            R.drawable.toy,
            R.drawable.toy
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);
        setImages();
        getSupportActionBar().hide();



        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showImageInitial();
                System.out.println("hiding images");
            }
        }, 1000);

//        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideImages();
                System.out.println("hiding images");
            }
        }, 2500);



    }

    public void goToNext(View v){
        Intent in = new Intent(this, Level2EnterActivity.class);
        startActivity(in);
    }

    public void goToNextActivity(){
        Intent in = new Intent(this, Level2EnterActivity.class);
        startActivity(in);
    }

    public void goTopreviousActivity(View v){
        Intent in = new Intent(this, Level1EnterActivity.class);
        startActivity(in);
    }


    public void showImage(View v){
        System.out.println("clicked an image");

        compareFlag++;
        ImageView img = (ImageView) findViewById(v.getId());
        System.out.println("img.getTag() "+Integer.valueOf(img.getTag().toString()));
        img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
        if(compareFlag==1) {
            actualImageV1 = img;
            imageV1 = img;
            System.out.println("you clicked img1");
        } else{
            actualImageV2 = img;
            imageV2 = img;
            System.out.println("you clicked img2");
            matchImages();

        }

    }



    public void matchImages(){

        b1=((BitmapDrawable)imageV1.getDrawable()).getBitmap();
        b2=((BitmapDrawable)imageV2.getDrawable()).getBitmap();
        if(b1==b2){
            totalMatchesPending-=2;
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    imageV1.setImageDrawable(null);
                    imageV2.setImageDrawable(null);
                    imageV1.setClickable(false);
                    imageV2.setClickable(false);
                    System.out.println("images matched");
                }
            }, 500);
            if(totalMatchesPending==0) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imageV1.setImageDrawable(null);
                        imageV2.setImageDrawable(null);
                        imageV1.setClickable(false);
                        imageV2.setClickable(false);
                        System.out.println("images matched");
                    }
                }, 500);
                goToNextActivity();
            }

        }
        else{
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    imageV1.setImageResource(R.drawable.questionmark);
                    imageV2.setImageResource(R.drawable.questionmark);
                    System.out.println("images not matched");
                }
            }, 500);

        }
        compareFlag=0;

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setImages(){
        System.out.println("in set images");
        ThreadLocalRandom rnd = ThreadLocalRandom.current();
        for (int i = images.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = images[index];
            images[index] = images[i];
            images[i] = a;
        }

        for(int i:images){
            System.out.println(i+" ");
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, Music.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, Music.class));
    }


    public void showImageInitial(){

        for(int i=0;i<9;i++){
            ImageView img = (ImageView) findViewById(R.id.image00);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image01);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image02);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image03);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image10);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image11);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image12);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image13);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image20);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image21);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image22);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);
            img = (ImageView) findViewById(R.id.image23);
            img.setImageResource(images[Integer.valueOf(img.getTag().toString())]);

        }

    }


    public void hideImages(){
        ImageView img = (ImageView) findViewById(R.id.image00);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image01);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image02);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image03);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image10);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image11);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image12);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image13);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image20);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image21);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image22);
        img.setImageResource(R.drawable.questionmark);
        img = (ImageView) findViewById(R.id.image23);
        img.setImageResource(R.drawable.questionmark);


    }


}
