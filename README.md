# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for the project on android platform. 

** Name of the project: FunZone - Kids fun learning app    
Team members:    
Naga Sravanthi Modali (S528749)  
Sreeja Reddy Challa (S528736)  
Sushmitha Madireddy (S528806)  
Venkata Naga Mahesh Kumar Vankayala (S528870)  **


### Application overview: ###

This app is an effective learning and fun tool for the kids. The app provides three types of games, whose degree of difficulty range from easy, medium and hard.

Below are various levels in the game:

Level 1: Match the numbers
The player will be provided with a list of cards on the screen. Player has to click on two cards on the screen. On clicking, the cards are flipped to show the number on them. If the numbers match, the cards are removed from the screen and user gets a point. If the cards do not match, the user loses a point. Now, the player has to repeat the same for the rest of the cards on the screen until all the cards get cleared from the screen.

Level 2: Test your Alphabet
The player is provided with three alphabets and three images, which starts with three letters randomly. The player should select one image at a time and place it on respective letter. When the image and the right letter is selected, the image is displayed along with the name of the item on the image. If the player matches all three images correctly, he/she goes to the next level. When an wrong match happens, a message is displayed saying the same and asked to give another try.

Level 3: Sequencing 
The player is given four alphabets randomly. There are two options available in the game. One, is to place them in order and second, is to place them in reverse order (in-order/reverse order chosen randomly by the game). If they are placed incorrectly then content in all the boxes is cleared and user is asked to place correctly. If the sequence is right, the score card is displayed and the player is prompted to go to the next level in the game.

Level 4: Test your memory
This is the final level in the game. In this level player memory will be tested by showing random alphabets and numbers through a grid view. After 4 seconds or so, the player is given with a blank grid to place the alphabet/numbers in the same fashion. If the player comes up with the right pattern, the player wins. If the pattern does not match, the player is given an option to try again. Once the player wins, the player is displayed the score of the game in the form of stars. 5 stars to 1star for the best play based on the timing and the player can choose to play the game all over again or exit the game.

Have fun learning :)


### How do I get set up? ###
under construction
 Install the APK and you are good to go.
 Link to apk: https://drive.google.com/open?id=1p_46ZE5LxQ8226VycxAV3I5EIzhs0icu

 Supported devices:
 All devices with android 5.1 or more and API 19
 Nexus_5X

### Contribution guidelines ###
** Sravanthi**  

 Created layout for level 1 and consolidated layouts for all the levels developed by team  
 Developed level1 layouts, game logic and implementation  
 Added 'skip level' functionality for all the levels  
 Generated APK for the app  
 
 ** Sreeja Reddy**  
 Contributed for Level 2: Test your Alphabet  
 Designed layout level 2 and entering to level 2  
 Developed functionality for level 2 game  
 Added app icon and conributed for home page design  
 
 **Sushmitha Madireddy**
 Contributed for Level 3: Sequencing 
 Designed layout level 3 and entering to level 3 
 Developed functionality for level 3 game
 Added backgroound music for the app
 
  **Mahesh Vankayala**
 Contributed for Level 4: Rememeber and Guess 
 Designed layout level 4 and entering to level 4 
 Developed functionality for level 4 game
 Added backgroound music for the app

### Who do I talk to? ###
under construction
* Repo owner or admin
* Other community or team contact